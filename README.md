# Coarse Grain Sparsity and quantization
The repository provides APIs to train Deep Neural Networks (DNN) with quantization and structured sparsity contraints. Weights and activations can be quantized to different low precision settings, such as 1-bit, 2-bit, 4-bit, 8-bit, 32-bit full precision etc. Structured sparsity contraints of Coarse Grain Sparsity (CGS) can be applied during training. This repository was inspired from BinaryNet (https://github.com/MatthieuCourbariaux/BinaryNet.git) and BinaryConnect (https://github.com/MatthieuCourbariaux/BinaryConnect.git) code. Additions are done to the code to add different low level precision constraints and CGS constraints while training. 

## Documentation
### Requirements
The project was generated with python 2.7.5. User would need to install following python libraries: 
>Theano, Lasagne, sys, six, os, time, numpy, argparse. The current code was validated with Theano- 0.10.0dev1.dev-RELEASE and lasagne-0.2.dev. 


### Args
The code can be run for specific settings. The setting can be changed by passing below arguments as parameters:
- `-ac`: destination variable='activation', default='relu', activation function (relu, binary(+1/-1), hard_fire(0,1), relu_2bit(0,1,2,3), ternary(0,1,2), relu_3bit(0,1,2,3,4,5,6,7), relu_4bit(0 to 15), relu_8bit(0 to 255)
- `-bs`, destination variable='batch_size', data type=int, default=50, batch size for training    
- `-al`, destination variable='alpha', data type=float, default=.1, alpha for batch normalization running mean
- `-ep`, destinaiton variable='epsilon', data type=float, default=1e-4, epsilon for batch normalization
- `-cz`, destinaiton variable='center_zero', data type=bool, default=True, normalize input to (-1,+1) if True, otherwise, to (0, 1) 
- `-n1`, destination variable='n_conv_1', data type=int, default=128, channel size at first two conv layers 
- `-n2`, destination variable='n_conv_2', data type=int, default=256, channel size at second two conv layers
- `-n3`, destination variable='n_conv_3', data type=int, default=512, channel size at third two conv layers
- `-pc`, destination variable='cgs_per_conv', data type=float, default=1.0, cgs percentage for conv layers, 1.0 means fully-connected and cgs is not applied
- `-pm`, destination variable='cgs_per_mlp', data type=float, default=1.0, cgs percentage for mlp layers, 1.0 means fully-connected and cgs is not applied
- `-bch`, destination variable='cgs_blkh_conv', data type=int, default=4, cgs block height for conv layers
- `-bcw`, destination variable='cgs_blkw_conv', data type=int, default=4, cgs block width for conv layers 
- `-bm`, destination variable='cgs_size_mlp', data type=int, default=16, cgs block size for mlp layers
- `-ef`, destination variable='equal_fanin', data type=bool, default=False, equal fanin for cgs (each neuron has equal fanin neurons) if True, equal fanout for cgs if False 
- `-eb`, destination variable='equal_both', data type=bool, default=False, for each neuron, it has equal fanin and equal fanout neurons if True, will override equal_fanin option 
- `-nu`, destination variable='num_units', data type=int, default=1024, number of neurons in hidden fc layers
- `-rl`, destination variable='repeat_layers', data type=int, default=2, number of repeat layers. When repeat layers is this means that net is C128:C128:C256el 
- `-qz`, destination variable='quantize', data type=bool, default=True, binarizing weights during feed-forward pass if True 
- `-ne`, destination variable='num_epochs', data type=int, default=500, number of epochs for training 
- `-ls`, destination variable='LR_start', data type=float, default=1e-3, start learning rate 
- `-lf`, destination variable='LR_fin', data type=float, default=3e-7, finish learning rate 
- `-sp`, destination variable='save_path', default='./data', save path for model parameters 
- `-lp`, destination variable='load_path', default=None, path for pre-trained model parameters, if not None, the pre-trained model will be loaded before training starts
- `-fl`, destination variable='fliplr', data type=bool, default=False, flip the input images horizontally if True 
- `-di`, destination variable='dropout_input', data type=float, default=0.0, dropout ratio input images 
- `-dc`, destination variable='dropout_conv', data type=float, default=0.0, dropout ratio for conv layers
- `-dm`, destination variable='dropout_mlp', data type=float, default=0.0, dropout ratio for mlp layers
- `-lo`, destination variable='loss', default='square_hinge', loss function (square_hinge, cubic_hinge, or cross_entropy) 
- `-of`, destination variable='output_function', default='linear', output function (linear, softmax)    
- `-cl`, destination variable='clipping', data type=bool, default=True, Clip the high-precision weights to [-1,1] if True 
- `-nb`, destination variable='n_bits', data type=int, default=1, weigh precision
- `-qs`, destination variable='quant_style', default="shift_bits", quantization style, options: uniform, shift_bits, symm 
- `-fs`, destination variable='filter_side', data type=int, default=3, size of filter

### Returns
The trained model and log file are saved to a folder named 'data'. As early stopping criterion is used, the model is saved for the epoch with best validation accuracy. The destination directory path can be changed by passing argument to parameter `-sp`.

## Usage

### Data Preparation
Tests were run on Cifar10 dataset. We used Pylearn2 to load Cifar10 dataset. There are number of other repositories  and libraries to load the dataset, feel free to use your method to generate dataset npy files. If you are generating the npy files yourself, follow only the last step as mentioned below to add the path in ~/.bashrc file. dBelow are the steps needed to prepare data:
* Download Cifar10 dataset from https://www.cs.toronto.edu/~kriz/cifar.html
* We used Pylearn2 to load Cifar10 data in pkl format. As Pylearn2 did not work with latest theano version. We used an older theano version for loading and saving dataset files (Theano 0.9.0).
* Load Cifar10 pkl dataset files and save in npy format. We found that loading dataset from a npy file was 10 times faster than loading from a pkl file. Run below command to perform last two steps. This will load the pkl script using Pylearn2 and then save npy format in a folder 'datasets' (if the folder is not present, a folder named 'datasets' will be created inside the directory of the cloned repository).
> `python load_save_datasets.py`
* Add dataset path in ~/.bashrc file to access the dataset from any from wherever the repository is clone. The environment variable will be used in the main script to locate dataset directory.
> `export DATASET_PATH=<directory path to the saved dataset files>`
My dataset export command looks something like this:
> `export DATASET_PATH=/home/gsrivas4/r/BinaryConnect-lasagne/cifar100/datasets/`


### Run the script
Below is a sample command to run the script. Below command selects activation: 2bit relu type, quantizaiton: True, number of weight bits 1, CGS percent: 25%, quantization style: shift_bits, dropout on conv layers: 20%, dropout on fully connected layers: 50%. Run the script from inside the directory of the cloned repository.
```
python cifar10_cgs.py -ac relu_2bit -qz True -nb 1 -pm  .25 -qs shift_bits -dc .2 -dm .5
```

### Sample screen log
A on-screen log will be generated similar to as shown below. A logfile will also be saved similar to the on-screen log. 
```
$ python cifar10_cgs.py -ac relu_2bit -qz True -nb 1 -pm  .25 -qs shift_bits -dc .2 -dm .5
Using cuDNN version 5110 on context None
Preallocating 811/8119 Mb (0.100000) on cuda0
Mapped name None to device cuda0: GeForce GTX 1070 (0000:04:00.0)
Namespace(LR_fin=3e-07, LR_start=0.001, activation='relu_2bit', alpha=0.1, batch_size=50, center_zero=True, cgs_blkh_conv=4, cgs_blkw_conv=4, cgs_per_conv=1.0, cgs_per_mlp=0.25, cgs_size_mlp=16, clipping=True, dropout_conv=0.2, dropou
t_input=0.0, dropout_mlp=0.5, epsilon=0.0001, equal_both=False, equal_fanin=False, filter_side=3, fliplr=False, load_path=None, loss='square_hinge', n_bits=1, n_conv_1=128, n_conv_2=256, n_conv_3=512, num_epochs=500, num_units=1024, o
utput_function='linear', quant_style='shift_bits', quantize=True, repeat_layers=2, same_blk=False, save_path='./data', shiftCGS=False, stochastic=False)
activation = binary_net.relu_2bit
loss = square hinge
H = 1.0
W_LR_scale = Glorot
Loading CIFAR-10 dataset...
Building the CNN...
output shape of layer1 (None, 128, 32, 32)
output shape of layer2 (None, 128, 16, 16)
output shape of layer3 (None, 256, 16, 16)
output shape of layer4 (None, 256, 8, 8)
output shape of layer5 (None, 512, 8, 8)
output shape of layer6 (None, 512, 4, 4)
output shape of layer7 (None, 1024)
output shape of layer8 (None, 1024)
/home/gsrivas4_1/envnew/lib/python2.7/site-packages/theano/tensor/basic.py:2114: UserWarning: theano.tensor.round() changed its default from `half_away_from_zero` to `half_to_even` to have the same default as NumPy. Use the Theano fla
g `warn.round=False` to disable this warning.
  "theano.tensor.round() changed its default from"
  W_LR_scale = 28.0357
  H = 1.0
  W_LR_scale = 39.1918
  H = 1.0
  W_LR_scale = 48.0
  H = 1.0
  W_LR_scale = 55.4256
  H = 1.0
  W_LR_scale = 67.8822
  H = 1.0
  W_LR_scale = 78.3837
  H = 1.0
  W_LR_scale = 39.1918
  H = 1.0
  W_LR_scale = 18.4752
  H = 1.0
  W_LR_scale = 26.2552
  H = 1.0
  Training...
  relu_2bit_qs_shift_bits_bi_nb1_bm_16x16_25_nt_128_256_512_1024_rl2_dc20_dm50
  Epoch 1 of 500 took 64.4514250755s
    LR:                            0.001
    training loss:                 0.721875849499
    validation loss:               0.316325588524
    validation error rate:         46.4599998891%
    best epoch:                    1
    best validation error rate:    46.4599998891%
    test loss:                     0.317114871591
    test error rate:               46.699999854%
  relu_2bit_qs_shift_bits_bi_nb1_bm_16x16_25_nt_128_256_512_1024_rl2_dc20_dm50
  ....
  ```

## References
* https://github.com/MatthieuCourbariaux/BinaryConnect.git
* https://github.com/MatthieuCourbariaux/BinaryNet.git








