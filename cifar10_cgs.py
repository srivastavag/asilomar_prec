# Copyright 2017    Shihui Yin, Gaurav Srivastava    Arizona State University

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
# WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
# MERCHANTABLITY OR NON-INFRINGEMENT.
# See the Apache 2 License for the specific language governing permissions and
# limitations under the License.

# Description: VGG-like CNN for CIFAR-10 with optional CGS and quantization
# Created on 10/02/2017

from __future__ import print_function

import sys
import os
import time

import numpy as np
np.random.seed(1234) # for reproducibility

import lasagne
import theano
import theano.tensor as T

import argparse
from ast import literal_eval as bool

import binary_net


from collections import OrderedDict

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="VGG-like CNN for CIFAR-10 with optional CGS and quantization")
    parser.add_argument('-sc', dest='shiftCGS', type=bool, default=False, help="shift cgs uses cgs blocks not deleting\
            all the weights of a filter (default: %(default)s)") 
    parser.add_argument('-sb', dest='same_blk', type=bool, default=False, help="same block is used in shiftCGS \
            to have the weight within a filter as visually seen. Else their will be 3 weights from each of 3 \
            different filter. (default: %(default)s)")
    parser.add_argument('-ac', dest='activation', default='relu', help="activation function (relu, binary(+1/-1), \
            hard_fire(0,1), relu_2bit(0,1,2,3), ternary(0,1,2), relu_3bit(0,1,2,3,4,5,6,7), relu_4bit(0 to 15), \
            relu_8bit(0 to 255) (default: %(default)s)")
    parser.add_argument('-bs', dest='batch_size', type=int, default=50, help="batch size for training (default: %(default)d)")
    parser.add_argument('-al', dest='alpha', type=float, default=.1, help="alpha for batch normalization running mean (default: %(default).1f)")
    parser.add_argument('-ep', dest='epsilon', type=float, default=1e-4, help="epsilon for batch normalization (default: %(default).2e)")
    parser.add_argument('-cz', dest='center_zero', type=bool, default=True, help="normalize input to (-1,+1) if True, otherwise, to (0, 1) (default: %(default)s)")
    parser.add_argument('-n1', dest='n_conv_1', type=int, default=128, help="channel size at first two conv layers (default: %(default)d)")
    parser.add_argument('-n2', dest='n_conv_2', type=int, default=256, help="channel size at second two conv layers (default: %(default)d)")
    parser.add_argument('-n3', dest='n_conv_3', type=int, default=512, help="channel size at third two conv layers (default: %(default)d)")
    parser.add_argument('-pc', dest='cgs_per_conv', type=float, default=1.0, help="cgs percentage for conv layers, 1.0 means fully-connected and cgs is not applied (default: %(default).1f)")
    parser.add_argument('-pm', dest='cgs_per_mlp', type=float, default=1.0, help="cgs percentage for mlp layers, 1.0 means fully-connected and cgs is not applied (default: %(default).1f)")
    parser.add_argument('-bch', dest='cgs_blkh_conv', type=int, default=4, help="cgs block height for conv layers (default: %(default)d)")
    parser.add_argument('-bcw', dest='cgs_blkw_conv', type=int, default=4, help="cgs block width for conv layers (default: %(default)d)")
    parser.add_argument('-bm', dest='cgs_size_mlp', type=int, default=16, help="cgs block size for mlp layers (default: %(default)d)")
    parser.add_argument('-ef', dest='equal_fanin', type=bool, default=False, help="equal fanin for cgs (each neuron has equal fanin neurons) if True, equal fanout for cgs if False (default: %(default)s)")
    parser.add_argument('-eb', dest='equal_both', type=bool, default=False, help="for each neuron, it has equal fanin and equal fanout neurons if True, will override equal_fanin option (default: %(default)s)")
    parser.add_argument('-nu', dest='num_units', type=int, default=1024, help="number of neurons in hidden fc layers (default: %(default)d)")
    parser.add_argument('-rl', dest='repeat_layers', type=int, default=2, help="number of repeat layers. When repeat\
            layers is this means that net is C128:C128:C256el (default : %(default)d)") 
    parser.add_argument('-st', dest='stochastic', type=bool, default=False, help="stochastic rounding for binarization of weights if True (default: %(default)s)")
    parser.add_argument('-qz', dest='quantize', type=bool, default=True, help="binarizing weights during feed-forward pass if True (default: %(default)s)")
    parser.add_argument('-ne', dest='num_epochs', type=int, default=500, help="number of epochs for training (default: %(default)d)")
    parser.add_argument('-ls', dest='LR_start', type=float, default=1e-3, help="start learning rate (default: %(default).1e)")
    parser.add_argument('-lf', dest='LR_fin', type=float, default=3e-7, help="finish learning rate (default: %(default).1e)")
    parser.add_argument('-sp', dest='save_path', default='./data', help="save path for model parameters (default: %(default)s)")
    parser.add_argument('-lp', dest='load_path', default=None, help="path for pre-trained model parameters, if not None, the pre-trained model will be loaded before training starts (default: %(default)s)")
    parser.add_argument('-fl', dest='fliplr', type=bool, default=False, help="flip the input images horizontally if True (default: %(default)s)")
    parser.add_argument('-di', dest='dropout_input', type=float, default=0.0, help="dropout ratio input images (default: %(default).1f)")    
    parser.add_argument('-dc', dest='dropout_conv', type=float, default=0.0, help="dropout ratio for conv layers (default: %(default).1f)")
    parser.add_argument('-dm', dest='dropout_mlp', type=float, default=0.0, help="dropout ratio for mlp layers (default: %(default).1f)")    
    parser.add_argument('-lo', dest='loss', default='square_hinge', help="loss function (square_hinge, cubic_hinge, or cross_entropy) (default: %(default)s)")
    parser.add_argument('-of', dest='output_function', default='linear', help="output function (linear, softmax) (default: %(default)s)")
    parser.add_argument('-cl', dest='clipping', type=bool, default=True, help="Clip the high-precision weights to [-1,1] if True (default: %(default)s)")
    parser.add_argument('-nb', dest='n_bits', type=int, default=1, help="weigh precision (default: %(default)s)")
    parser.add_argument('-qs', dest='quant_style', default="shift_bits", help="quantization style, options: uniform,\
            shift_bits, symm (default: %(default)s)")
    parser.add_argument('-fs', dest='filter_side', type=int, default=3, help="size of filter (default: %(default)d)")
    args = parser.parse_args()
    print(args)
    
    
    # Network  parameters
    batch_size = args.batch_size #50
    alpha = args.alpha #.1
    epsilon = args.epsilon #1e-4
    
    n_conv_1 = args.n_conv_1 #128
    n_conv_2 = args.n_conv_2 #256
    n_conv_3 = args.n_conv_3 #512
    num_units = args.num_units #1024
    
    # CGS parameters
    shiftCGS = args.shiftCGS
    cgs_per_conv = args.cgs_per_conv #1.0
    cgs_per_mlp = args.cgs_per_mlp #1.0
    
    # cgs block width corresponds to the number of input layer size or the number of channels in the filter.
    # when cgs mask is created, it is assumed that width is along the number of channels in the filter. Another term
    # used is the number of feature maps in the input conv layer, which will be same as number of channels in the 
    # filter. 
    cgs_blkw_conv = args.cgs_blkw_conv #4
    # block size along the height of the mask is formation of block along the number of filter dimensions, which 
    # equals the number of channels in output conv layer. block height is the height or number of filters covered 
    # with one block.
    cgs_blkh_conv = args.cgs_blkh_conv #4
    cgs_size_mlp = args.cgs_size_mlp #16
    filter_side = args.filter_side
    equal_fanin = args.equal_fanin #False
    equal_both = args.equal_both # False
    
    save_path = args.save_path
    
    W_mask_list=[]
    W_list=[]
    
    # Quantized activation
    if args.activation == 'binary':
        activation = binary_net.binary_tanh_unit
        print("activation = binary_net.binary_tanh_unit")
    if args.activation == 'ternary':
        activation = binary_net.ternary_tanh_unit
        print("activation = binary_net.ternary_tanh_unit")
    if args.activation == 'relu' or args.activation == 'rectify':
        activation = lasagne.nonlinearities.rectify
        print("activation = lasagne.nonlinearities.rectify")
    if args.activation == 'hard_fire':
        activation = binary_net.hard_fire
        print("activation = binary_net.hard_fire")
    if args.activation == 'hard_fire2':
        activation = binary_net.hard_fire2
        print("activation = binary_net.hard_fire2")
    if args.activation == 'ternary_relu':
        activation = binary_net.ternary_relu
        print("activation = binary_net.ternary_relu")
    if args.activation == 'ternary_relu2':
        activation = binary_net.ternary_relu2
        print("activation = binary_net.ternary_relu2")    
    if args.activation == 'relu_2bit':
        activation = binary_net.relu_2bit
        print("activation = binary_net.relu_2bit")   
    if args.activation == 'relu_3bit':
        activation = binary_net.relu_3bit
        print("activation = binary_net.relu_3bit")  
    if args.activation == 'relu_4bit':
        activation = binary_net.relu_4bit
        print("activation = binary_net.relu_4bit")
    if args.activation == 'relu_8bit':
        activation = binary_net.relu_8bit
        print("activation = binary_net.relu_8bit")
    if args.activation == 'relu_12bit':
        activation = binary_net.relu_12bit
        print("activation = binary_net.relu_12bit")
    if args.activation == 'relu_16bit':
        activation = binary_net.relu_16bit
        print("activation = binary_net.relu_16bit")
    if args.activation == 'relu_32bit':
        activation = binary_net.relu_32bit
        print("activation = binary_net.relu_32bit")

    if args.loss == 'square_hinge':
        print("loss = square hinge")
    if args.loss == 'cubic_hinge':
        print("loss = cubic hinge")
    if args.loss == 'neg_log_likelihood': # not implemented yet
        print("loss = negative log likelihood")
    if args.loss == 'cross_entropy':
        print("loss = cross entropy")
    n_bits = args.n_bits
    quant_style = args.quant_style 
    
    # Below parameters are taken from orginal BinaryConnect code 
    quantize = args.quantize #True
    stochastic = args.stochastic # Hard coded to False
    # H = "Glorot"
    H = 1.
    print("H = "+str(H))
    # W_LR_scale = 1.    
    W_LR_scale = "Glorot" # "Glorot" means we are using the coefficients from Glorot's paper
    print("W_LR_scale = "+str(W_LR_scale))
    
    # Training parameters
    num_epochs = args.num_epochs #500
    # print("num_epochs = "+str(num_epochs))
    
    # Decaying LR 
    LR_start = args.LR_start #0.001
    # print("LR_start = "+str(LR_start))
    LR_fin = args.LR_fin #0.0000003
    # print("LR_fin = "+str(LR_fin))
    LR_decay = (LR_fin/LR_start)**(1./num_epochs)
    # print("LR_decay = "+str(LR_decay))
    
    train_set_size = 45000
    # print("train_set_size = "+str(train_set_size))
    shuffle_parts = 1
    # print("shuffle_parts = "+str(shuffle_parts))
    # Flip the images
    fliplr = args.fliplr
    
    # Generate a unique name for test with current settings
    arch_name = '{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}'.format('{}'.format(args.activation),\
            '_fs{}'.format(filter_side) if args.filter_side!=3 else '',\
            '_qs_{}'.format(quant_style) if quantize==True else '',\
            '_bi' if quantize==True else '',\
            '_nb{}'.format(n_bits) if quantize==True else '',\
            '_sc' if (cgs_per_conv != 1.) and shiftCGS else '',\
            '_sb' if (cgs_per_conv !=1.) and shiftCGS and args.same_blk else '',\
            '_wf{}'.format(args.num_within) if cgs_per_conv !=1. and args.shiftCGS and args.withinfil else '',\
            '_bc{}x{}_{}'.format(cgs_blkh_conv, cgs_blkw_conv, int(cgs_per_conv*100)) if cgs_per_conv!=1 else '',\
            '_bm_{}x{}_{}'.format(cgs_size_mlp, cgs_size_mlp, int(cgs_per_mlp*100)) if cgs_per_mlp!=1 else '',\
            '_nt_{}_{}_{}_{}'.format(n_conv_1, n_conv_2, n_conv_3, num_units),\
            '_rl{}'.format(args.repeat_layers),\
            '_di{}'.format(int(args.dropout_input*100)) if args.dropout_input>0. else '',\
            '_dc{}'.format(int(args.dropout_conv*100)) if args.dropout_conv>0. else '',\
            '_dm{}'.format(int(args.dropout_mlp*100)) if args.dropout_mlp>0. else '', 
            '_bs{}'.format(batch_size) if batch_size!=50 else ''
            )
    print('Loading CIFAR-10 dataset...')
    #train_set = CIFAR10(which_set="train",start=0,stop = train_set_size)
    #valid_set = CIFAR10(which_set="train",start=train_set_size,stop = 50000)
    #test_set = CIFAR10(which_set="test")
    
    dataset_path = os.environ['DATASET_PATH']

    train_set_x = np.load('{}/cifar10_train_x.npy'.format(dataset_path))
    train_set_y = np.load('{}/cifar10_train_y.npy'.format(dataset_path))
    valid_set_x = np.load('{}/cifar10_valid_x.npy'.format(dataset_path))
    valid_set_y = np.load('{}/cifar10_valid_y.npy'.format(dataset_path))
    test_set_x = np.load('{}/cifar10_test_x.npy'.format(dataset_path))
    test_set_y = np.load('{}/cifar10_test_y.npy'.format(dataset_path))

    # bc01 format
    # Inputs in the range [-1,+1]
    # print("Inputs in the range [-1,+1]")
  
    #train_set_x = np.reshape(np.subtract(np.multiply(2./255.,train_set_x),1.),(-1,3,32,32))
    #valid_set_x = np.reshape(np.subtract(np.multiply(2./255.,valid_set_x),1.),(-1,3,32,32))
    #test_set_x = np.reshape(np.subtract(np.multiply(2./255.,test_set_x),1.),(-1,3,32,32))
    
    # Adding below three lines. Along with the center_zero this will convert pixel values [-1, +1]
    train_set_x = np.reshape(np.multiply(1./255.,train_set_x),(-1,3,32,32))
    valid_set_x = np.reshape(np.multiply(1./255.,valid_set_x),(-1,3,32,32))
    test_set_x = np.reshape(np.multiply(1./255.,test_set_x),(-1,3,32,32))
   
    if args.center_zero == True:
        train_set_x = 2. * train_set_x - 1.
        valid_set_x = 2. * valid_set_x - 1.
        test_set_x = 2. * test_set_x - 1.
    
    # flatten targets
    train_set_y = np.hstack(train_set_y)
    valid_set_y = np.hstack(valid_set_y)
    test_set_y = np.hstack(test_set_y)
    
    # Onehot the targets
    train_set_y = np.float32(np.eye(10)[train_set_y])    
    valid_set_y = np.float32(np.eye(10)[valid_set_y])
    test_set_y = np.float32(np.eye(10)[test_set_y])
    
    # for hinge loss
    if args.loss == 'square_hinge':
        train_set_y = 2* train_set_y - 1.
        valid_set_y = 2* valid_set_y - 1.
        test_set_y = 2* test_set_y - 1.

    print('Building the CNN...') 
    
    # Prepare Theano variables for inputs and targets
    input = T.tensor4('inputs')
    target = T.matrix('targets')
    LR = T.scalar('LR', dtype=theano.config.floatX)

    cnn = lasagne.layers.InputLayer(
            shape=(None, 3, 32, 32),
            input_var=input)
    if args.dropout_input > 0.0:
        cnn = lasagne.layers.DropoutLayer(
                cnn, 
                p=args.dropout_input)
    count_layers = 0
    # 128C3-128C3-P2             
    # First conv layer applied on the input is non-sparse
    cnn = binary_net.Conv2DLayer(
            cnn, 
            quantize=quantize,
            stochastic=stochastic,
            H=H,
            W_LR_scale=W_LR_scale,
            num_filters=n_conv_1, 
            filter_size=(filter_side, filter_side),
            pad='same',
            nonlinearity=lasagne.nonlinearities.identity,
            n_bits=n_bits,
            quant_style=quant_style)      
    
    # if repeated layers is 1, then need to apply max pooling on the first layer. If repeated layers are 2 or more 
    # max pool will be applied on last layer of the first batch of repeated layers.
    if args.repeat_layers==1:
        cnn = lasagne.layers.MaxPool2DLayer(cnn, pool_size=(2, 2))
    
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
    
    if args.dropout_conv > 0.0:
        cnn = lasagne.layers.DropoutLayer(
                cnn, 
                p=args.dropout_conv)
    count_layers += 1
    print('output shape of layer{} {}'.format(count_layers, lasagne.layers.get_output_shape(cnn)))
    
    for ii in range(args.repeat_layers-2):
        if shiftCGS==True:        
            cnn = binary_net.Conv2DLayer_shiftCGS(
                cnn, 
                quantize=quantize,
                num_filters=n_conv_1, filter_size=(filter_side, filter_side),
                cgs_blkh_conv=cgs_blkh_conv,
                cgs_blkw_conv=cgs_blkw_conv,
                cgs_percentage=cgs_per_conv,
                n_bits=n_bits,
                quant_style=quant_style,      
                equal_fanin=equal_fanin,
                equal_both=equal_both,
                nonlinearity=lasagne.nonlinearities.identity,
                W_LR_scale=W_LR_scale, stochastic=False, pad='same',
                H=H,
                same_blk = args.same_blk,
                withinfil = args.withinfil, num_within = args.num_within)
        else:
            cnn = binary_net.Conv2DLayer_CGS(
                cnn, 
                quantize=quantize,
                num_filters=n_conv_1, filter_size=(filter_side, filter_side),
                cgs_blkh_conv=cgs_blkh_conv,
                cgs_blkw_conv=cgs_blkw_conv,
                cgs_percentage=cgs_per_conv,
                n_bits=n_bits,
                quant_style=quant_style,      
                equal_fanin=equal_fanin,
                equal_both=equal_both,
                nonlinearity=lasagne.nonlinearities.identity,
                H=H,
                W_LR_scale=W_LR_scale, stochastic=False, pad='same')
        
        params = cnn.get_params()
        for param in params:
            if param.name == "W":
                W_list.append(param)
        W_mask_list.append(cnn.W_mask)
        
        cnn = lasagne.layers.BatchNormLayer(
                cnn,
                epsilon=epsilon, 
                alpha=alpha)
                    
        cnn = lasagne.layers.NonlinearityLayer(
                cnn,
                nonlinearity=activation) 
        
        if args.dropout_conv > 0.0:
            cnn = lasagne.layers.DropoutLayer(
                    cnn, 
                    p=args.dropout_conv)
        count_layers += 1
        print('output shape of layer{} {}'.format(count_layers, lasagne.layers.get_output_shape(cnn)))
    
    if args.repeat_layers>1:
        if shiftCGS==True:        
            cnn = binary_net.Conv2DLayer_shiftCGS(
                cnn, 
                quantize=quantize,
                num_filters=n_conv_1, filter_size=(filter_side, filter_side),
                cgs_blkh_conv=cgs_blkh_conv,
                cgs_blkw_conv=cgs_blkw_conv,
                cgs_percentage=cgs_per_conv,
                n_bits=n_bits,
                quant_style=quant_style,      
                equal_fanin=equal_fanin,
                equal_both=equal_both,
                nonlinearity=lasagne.nonlinearities.identity,
                W_LR_scale=W_LR_scale, stochastic=False, pad='same',
                H=H,
                same_blk = args.same_blk,
                withinfil = args.withinfil, num_within = args.num_within)
        else:
            cnn = binary_net.Conv2DLayer_CGS(
                cnn, 
                quantize=quantize,
                num_filters=n_conv_1, filter_size=(filter_side, filter_side),
                cgs_blkh_conv=cgs_blkh_conv,
                cgs_blkw_conv=cgs_blkw_conv,
                cgs_percentage=cgs_per_conv,
                n_bits=n_bits,
                quant_style=quant_style,      
                equal_fanin=equal_fanin,
                equal_both=equal_both,
                nonlinearity=lasagne.nonlinearities.identity,
                H=H,
                W_LR_scale=W_LR_scale, stochastic=False, pad='same')
        params = cnn.get_params()
        for param in params:
            if param.name == "W":
                W_list.append(param)
        W_mask_list.append(cnn.W_mask)
        
        cnn = lasagne.layers.MaxPool2DLayer(cnn, pool_size=(2, 2))
        
        cnn = lasagne.layers.BatchNormLayer(
                cnn,
                epsilon=epsilon, 
                alpha=alpha)
                    
        cnn = lasagne.layers.NonlinearityLayer(
                cnn,
                nonlinearity=activation) 
        
        if args.dropout_conv > 0.0:
            cnn = lasagne.layers.DropoutLayer(
                    cnn, 
                    p=args.dropout_conv)

        count_layers += 1
        print('output shape of layer{} {}'.format(count_layers , lasagne.layers.get_output_shape(cnn)))
            
    # 256C3-256C3-P2             
    # if repeated layers is just 1, then need to apply max pooling on the first layer. If repeated layers are 2 or more 
    # max pool will be applied on last layer of the first batch of repeated layers.
    if args.repeat_layers==1:
        cnn = lasagne.layers.MaxPool2DLayer(cnn, pool_size=(2, 2))
    for ii2 in range(args.repeat_layers-1):
        if shiftCGS==True:        
            cnn = binary_net.Conv2DLayer_shiftCGS(
                cnn, 
                quantize=quantize,
                num_filters=n_conv_2, filter_size=(filter_side, filter_side),
                cgs_blkh_conv=cgs_blkh_conv,
                cgs_blkw_conv=cgs_blkw_conv,
                cgs_percentage=cgs_per_conv,
                n_bits=n_bits,
                quant_style=quant_style,      
                equal_fanin=equal_fanin,
                equal_both=equal_both,
                nonlinearity=lasagne.nonlinearities.identity,
                W_LR_scale=W_LR_scale, stochastic=False, pad='same',
                H=H,
                same_blk = args.same_blk,
                withinfil = args.withinfil, num_within = args.num_within)
        else:
            cnn = binary_net.Conv2DLayer_CGS(
                cnn, 
                quantize=quantize,
                num_filters=n_conv_2, filter_size=(filter_side, filter_side),
                cgs_blkh_conv=cgs_blkh_conv,
                cgs_blkw_conv=cgs_blkw_conv,
                cgs_percentage=cgs_per_conv,
                n_bits=n_bits,
                quant_style=quant_style,
                equal_fanin=equal_fanin,
                equal_both=equal_both,
                nonlinearity=lasagne.nonlinearities.identity,
                H=H,
                W_LR_scale=W_LR_scale, stochastic=False, pad='same')
        params = cnn.get_params()
        for param in params:
            if param.name == "W":
                W_list.append(param)
        W_mask_list.append(cnn.W_mask)
        
        cnn = lasagne.layers.BatchNormLayer(
                cnn,
                epsilon=epsilon, 
                alpha=alpha)
                    
        cnn = lasagne.layers.NonlinearityLayer(
                cnn,
                nonlinearity=activation) 
        
        if args.dropout_conv > 0.0:
            cnn = lasagne.layers.DropoutLayer(
                    cnn, 
                    p=args.dropout_conv)
        count_layers += 1
        print('output shape of layer{} {}'.format(count_layers , lasagne.layers.get_output_shape(cnn)))
            
    if shiftCGS==True:        
        cnn = binary_net.Conv2DLayer_shiftCGS(
            cnn, 
            quantize=quantize,
            num_filters=n_conv_2, filter_size=(filter_side, filter_side),
            cgs_blkh_conv=cgs_blkh_conv,
            cgs_blkw_conv=cgs_blkw_conv,
            cgs_percentage=cgs_per_conv,
            n_bits=n_bits,
            quant_style=quant_style,      
            equal_fanin=equal_fanin,
            equal_both=equal_both,
            nonlinearity=lasagne.nonlinearities.identity,
            H=H,
            W_LR_scale=W_LR_scale, stochastic=False, pad='same',
            same_blk = args.same_blk,
            withinfil = args.withinfil, num_within = args.num_within)
    else:
        cnn = binary_net.Conv2DLayer_CGS(
            cnn, 
            quantize=quantize,
            num_filters=n_conv_2, filter_size=(filter_side, filter_side),
            cgs_blkh_conv=cgs_blkh_conv,
            cgs_blkw_conv=cgs_blkw_conv,
            cgs_percentage=cgs_per_conv,
            n_bits=n_bits,
            quant_style=quant_style,      
            equal_fanin=equal_fanin,
            equal_both=equal_both,
            nonlinearity=lasagne.nonlinearities.identity,
            H=H,
            W_LR_scale=W_LR_scale, stochastic=False, pad='same')
    params = cnn.get_params()
    for param in params:
        if param.name == "W":
            W_list.append(param)
    W_mask_list.append(cnn.W_mask)
    
    cnn = lasagne.layers.MaxPool2DLayer(cnn, pool_size=(2, 2))
    
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)
                
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
    
    if args.dropout_conv > 0.0:
        cnn = lasagne.layers.DropoutLayer(
                cnn, 
                p=args.dropout_conv)
    
    count_layers += 1
    print('output shape of layer{} {}'.format(count_layers , lasagne.layers.get_output_shape(cnn)))
    
    # 512C3-512C3-P2              
    for ii in range(args.repeat_layers-1):
        if shiftCGS==True:        
            cnn = binary_net.Conv2DLayer_shiftCGS(
                cnn, 
                quantize=quantize,
                num_filters=n_conv_3, filter_size=(filter_side, filter_side),
                cgs_blkh_conv=cgs_blkh_conv,
                cgs_blkw_conv=cgs_blkw_conv,
                cgs_percentage=cgs_per_conv,
                n_bits=n_bits,
                quant_style=quant_style,      
                equal_fanin=equal_fanin,
                equal_both=equal_both,
                nonlinearity=lasagne.nonlinearities.identity,
                H=H,
                W_LR_scale=W_LR_scale, stochastic=False, pad='same',
                same_blk = args.same_blk,
                withinfil = args.withinfil, num_within = args.num_within)
        else:
            cnn = binary_net.Conv2DLayer_CGS(
                cnn, 
                quantize=quantize,
                num_filters=n_conv_3, filter_size=(filter_side, filter_side),
                cgs_blkh_conv=cgs_blkh_conv,
                cgs_blkw_conv=cgs_blkw_conv,
                cgs_percentage=cgs_per_conv,
                n_bits=n_bits,
                quant_style=quant_style,      
                equal_fanin=equal_fanin,
                equal_both=equal_both,
                nonlinearity=lasagne.nonlinearities.identity,
                H=H,
                W_LR_scale=W_LR_scale, stochastic=False, pad='same')
        params = cnn.get_params()
        for param in params:
            if param.name == "W":
                W_list.append(param)
        W_mask_list.append(cnn.W_mask)
        
        cnn = lasagne.layers.BatchNormLayer(
                cnn,
                epsilon=epsilon, 
                alpha=alpha)
                    
        cnn = lasagne.layers.NonlinearityLayer(
                cnn,
                nonlinearity=activation) 
        if args.dropout_conv > 0.0:
            cnn = lasagne.layers.DropoutLayer(
                    cnn, 
                    p=args.dropout_conv)
    
        count_layers += 1
        print('output shape of layer{} {}'.format(count_layers , lasagne.layers.get_output_shape(cnn)))
                  
    if shiftCGS==True:        
        cnn = binary_net.Conv2DLayer_shiftCGS(
            cnn, 
            quantize=quantize,
            num_filters=n_conv_3, filter_size=(filter_side, filter_side),
            cgs_blkh_conv=cgs_blkh_conv,
            cgs_blkw_conv=cgs_blkw_conv,
            cgs_percentage=cgs_per_conv,
            n_bits=n_bits,
            quant_style=quant_style,      
            equal_fanin=equal_fanin,
            equal_both=equal_both,
            nonlinearity=lasagne.nonlinearities.identity,
            H=H,
            W_LR_scale=W_LR_scale, stochastic=False, pad='same',
            same_blk = args.same_blk,
            withinfil = args.withinfil, num_within = args.num_within)
    else:
        cnn = binary_net.Conv2DLayer_CGS(
            cnn, 
            quantize=quantize,
            num_filters=n_conv_3, filter_size=(filter_side, filter_side),
            cgs_blkh_conv=cgs_blkh_conv,
            cgs_blkw_conv=cgs_blkw_conv,
            cgs_percentage=cgs_per_conv,
            n_bits=n_bits,
            quant_style=quant_style,      
            equal_fanin=equal_fanin,
            equal_both=equal_both,
            nonlinearity=lasagne.nonlinearities.identity,
            H=H,
            W_LR_scale=W_LR_scale, stochastic=False, pad='same')
    params = cnn.get_params()
    for param in params:
        if param.name == "W":
            W_list.append(param)
    W_mask_list.append(cnn.W_mask)
    
    cnn = lasagne.layers.MaxPool2DLayer(cnn, pool_size=(2, 2))
    
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)
                
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
    
    # print(cnn.output_shape)
    count_layers += 1
    print('output shape of layer{} {}'.format(count_layers , lasagne.layers.get_output_shape(cnn)))
    
    # 1024FP-1024FP-10FP    
    # dropout between flatenning layer and fc layer
    if args.dropout_mlp > 0.0:
        cnn = lasagne.layers.DropoutLayer(
                cnn, 
                p=args.dropout_mlp)

    for ii in range(args.repeat_layers-1):
        cnn = binary_net.DenseLayer_CGS(
                cnn, 
                quantize=quantize,
                cgs_blk_size=cgs_size_mlp,
                cgs_percentage=cgs_per_mlp,
                equal_fanin=equal_fanin,
                equal_both=equal_both,
                nonlinearity=lasagne.nonlinearities.identity,
                H=H,
                W_LR_scale=W_LR_scale, stochastic=False,
                num_units=num_units,      
                n_bits=n_bits,
                quant_style=quant_style)      
        params = cnn.get_params()
        for param in params:
            if param.name == "W":
                W_list.append(param)
        W_mask_list.append(cnn.W_mask)
        
        cnn = lasagne.layers.BatchNormLayer(
                cnn,
                epsilon=epsilon, 
                alpha=alpha)
                    
        cnn = lasagne.layers.NonlinearityLayer(
                cnn,
                nonlinearity=activation) 
        if args.dropout_mlp > 0.0:
            cnn = lasagne.layers.DropoutLayer(
                    cnn, 
                    p=args.dropout_mlp)
        count_layers += 1
        print('output shape of layer{} {}'.format(count_layers , lasagne.layers.get_output_shape(cnn)))
                
    cnn = binary_net.DenseLayer_CGS(
            cnn, 
            quantize=quantize,
            cgs_blk_size=cgs_size_mlp,
            cgs_percentage=cgs_per_mlp,
            equal_fanin=equal_fanin,
            equal_both=equal_both,
            nonlinearity=lasagne.nonlinearities.identity,
            H=H,
            W_LR_scale=W_LR_scale, stochastic=False,
            num_units=num_units,      
            n_bits=n_bits,
            quant_style=quant_style)      
    params = cnn.get_params()
    for param in params:
        if param.name == "W":
            W_list.append(param)
    W_mask_list.append(cnn.W_mask)
    
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)
                
    cnn = lasagne.layers.NonlinearityLayer(
            cnn,
            nonlinearity=activation) 
    if args.dropout_mlp > 0.0:
        cnn = lasagne.layers.DropoutLayer(
                cnn, 
                p=args.dropout_mlp)
    count_layers += 1
    print('output shape of layer{} {}'.format(count_layers , lasagne.layers.get_output_shape(cnn)))
            
    cnn = binary_net.DenseLayer(
            cnn, 
            quantize=quantize,
            stochastic=stochastic,
            H=H,
            W_LR_scale=W_LR_scale,
            nonlinearity=lasagne.nonlinearities.identity,
            num_units=10,
            n_bits=n_bits,
            quant_style=quant_style)      
                  
    cnn = lasagne.layers.BatchNormLayer(
            cnn,
            epsilon=epsilon, 
            alpha=alpha)

    if args.output_function == 'softmax':
        cnn = lasagne.layers.NonlinearityLayer(
                cnn,
                nonlinearity=lasagne.nonlinearities.softmax) 
            
    train_output = lasagne.layers.get_output(cnn, deterministic=False)
    # squared hinge loss
    if args.loss == 'square_hinge':
        loss = T.mean(T.sqr(T.maximum(0.,1.-target*train_output)))
    if args.loss == 'cubic_hinge':
        loss = T.mean((T.maximum(0.,1.-target*train_output))**3)
    if args.loss == 'cross_entropy':
        loss = T.mean(lasagne.objectives.categorical_crossentropy(train_output, target))
    
    if quantize:
        
        # W updates
        W = lasagne.layers.get_all_params(cnn, quantize=True)
        W_grads = binary_net.compute_grads(loss,cnn)
        updates = lasagne.updates.adam(loss_or_grads=W_grads, params=W, learning_rate=LR)
        if args.clipping == True:
            updates = binary_net.clipping_scaling(updates,cnn)
        else:
            updates = binary_net.scaling(updates,cnn)
        
        # other parameters updates
        params = lasagne.layers.get_all_params(cnn, trainable=True, quantize=False)
        updates = OrderedDict(updates.items() + lasagne.updates.adam(loss_or_grads=loss, params=params, learning_rate=LR).items())
        
    else:
        params = lasagne.layers.get_all_params(cnn, trainable=True)
        updates = lasagne.updates.adam(loss_or_grads=loss, params=params, learning_rate=LR)
   
    if cgs_per_conv < 1.0:
        for W, W_mask in zip(W_list, W_mask_list):
            updates[W] = updates[W] * W_mask
        
    test_output = lasagne.layers.get_output(cnn, deterministic=True)
    if args.loss == 'square_hinge':
        test_loss = T.mean(T.sqr(T.maximum(0.,1.-target*test_output)))
    if args.loss == 'cubic_hinge':
        test_loss = T.mean((T.maximum(0.,1.-target*test_output))**3)
    if args.loss == 'cross_entropy':
        test_loss = T.mean(lasagne.objectives.categorical_crossentropy(test_output, target))
    test_err = T.mean(T.neq(T.argmax(test_output, axis=1), T.argmax(target, axis=1)),dtype=theano.config.floatX)
    
    # Compile a function performing a training step on a mini-batch (by giving the updates dictionary) 
    # and returning the corresponding training loss:
    train_fn = theano.function([input, target, LR], loss, updates=updates)

    # Compile a second function computing the validation loss and accuracy:
    val_fn = theano.function([input, target], [test_loss, test_err])

    if args.load_path is not None:
        print("Loading pretrained params...")
        with np.load(args.load_path) as f:
            W_values = [f['arr_%d' % i] for i in range(len(f.files))]
        params = lasagne.layers.get_all_params(cnn)
        for i in range(len(params)):
            params[i].set_value(W_values[i])
    
    print('Training...')
    
    binary_net.train(arch_name,
            train_fn,val_fn,
            cnn,
            batch_size,
            LR_start,LR_decay,
            num_epochs,
            train_set_x,train_set_y,
            valid_set_x,valid_set_y,
            test_set_x,test_set_y,
            save_path='./data',
            shuffle_parts=shuffle_parts,
            fliplr=fliplr)
