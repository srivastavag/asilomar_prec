python cifar10_cgs.py -ac relu -qz True -nb 1 -pm  1. -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 1 -pm  .5 -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 1 -pm  .25 -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 1 -pm  .125 -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 1 -pm  .0625 -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 2 -pm  1. -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 2 -pm  .5 -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 2 -pm  .25 -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 2 -pm  .125 -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 2 -pm  .0625 -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 3 -pm  1. -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 3 -pm  .5 -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 3 -pm  .25 -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 3 -pm  .125 -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz True -nb 3 -pm  .0625 -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz False -pm  1. -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz False -pm  .5 -qs shift_bits -dc .2 -dm .2 
python cifar10_cgs.py -ac relu -qz False -pm  .25 -qs shift_bits -dc .2 -dm .2
python cifar10_cgs.py -ac relu -qz False -pm  .125 -qs shift_bits -dc .2 -dm .2 
python cifar10_cgs.py -ac relu -qz False -pm  .0625 -qs shift_bits -dc .2 -dm .2
